/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsystem_real;

/**
 *
 * @author student
 */
public class PaySlip {
    private PhilHealth philhealth;
    private SSS sss;
    private PAGIBIG pagibig;
    private TaxPayment taxpayment;
    private OverTime overtimePay;
    private double salary;

    public PaySlip() {
    }

    public PaySlip(PhilHealth philhealth, SSS sss, PAGIBIG pagibig, TaxPayment taxpayment, OverTime overtimePay, double salary) {
        this.philhealth = philhealth;
        this.sss = sss;
        this.pagibig = pagibig;
        this.taxpayment = taxpayment;
        this.overtimePay = overtimePay;
        this.salary = salary;
    }

    public PhilHealth getPhilhealth() {
        return philhealth;
    }

    public void setPhilhealth(PhilHealth philhealth) {
        this.philhealth = philhealth;
    }

    public SSS getSss() {
        return sss;
    }

    public void setSss(SSS sss) {
        this.sss = sss;
    }

    public PAGIBIG getPagibig() {
        return pagibig;
    }

    public void setPagibig(PAGIBIG pagibig) {
        this.pagibig = pagibig;
    }

    public TaxPayment getTaxpayment() {
        return taxpayment;
    }

    public void setTaxpayment(TaxPayment taxpayment) {
        this.taxpayment = taxpayment;
    }

    public OverTime getOvertimePay() {
        return overtimePay;
    }

    public void setOvertimePay(OverTime overtimePay) {
        this.overtimePay = overtimePay;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "PaySlip{" + "philhealth=" + philhealth + ", sss=" + sss + ", pagibig=" + pagibig + ", taxpayment=" + taxpayment + ", overtime=" + overtimePay + ", salary=" + salary + '}';
    }
    
    
}
