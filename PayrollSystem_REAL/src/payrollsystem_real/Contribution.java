/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsystem_real;

/**
 *
 * @author student
 */
public class Contribution {
    private String contributionName;
    private double contribution;

    public Contribution() {
    }

    public Contribution(String contributionName, double contribution) {
        this.contributionName = contributionName;
        this.contribution = contribution;
    }

    public String getContributionName() {
        return contributionName;
    }

    public void setContributionName(String contributionName) {
        this.contributionName = contributionName;
    }

    public double getContribution() {
        return contribution;
    }

    public void setContribution(double contribution) {
        this.contribution = contribution;
    }

    @Override
    public String toString() {
        return "Contribution{" + "contributionName=" + contributionName + ", contribution=" + contribution + '}';
    }
    
}
