/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsystem_real;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author student
 */
public class Person {

    private String fName;
    private String mName;
    private String lName;
    private Date birthday;
    private int age;

    public Person() {
    }

    public Person(String fName, String mName, String lName, Date birthday, int age) {
        this.fName = fName;
        this.mName = mName;
        this.lName = lName;
        this.birthday = birthday;
        this.age = age;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
//        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
//        String date = sdf.format(birthday);
//        System.out.println(date); //15/10/2013
        this.birthday = birthday;
    }

    public int getAge() {
        Date date = new Date();
        age = date.getYear() + 1900 - birthday.getYear();
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder(); 
        String[] monthName = {"January", "February","March","April","May","June","July","August","September","October","November","December"};
        sb.append("\nName: " + fName);
        sb.append(" ");
        if (!(mName.equals("") || mName == null)) {
            sb.append(mName.charAt(0));
            sb.append(". ");
        }
        sb.append(lName);
        sb.append(String.format("\nBirthday: %s %d, %d " ,monthName[getBirthday().getMonth()-1],getBirthday().getDate(),getBirthday().getYear()));
        sb.append("\nAge: " + getAge());

        return sb.toString();
    }
}
