/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsystem_real;

/**
 *
 * @author student
 */
public class OverTime {

    private int days = 11;
    private int hours = 8;
    private double salary = 5000;
    private double rate = 0.30;

    public OverTime() {
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double overtimePay(int hrs) {
        int overtime = 0;
        double overtimePay = 0;
        double dailyrate;
        double ratePerHour;

        if (hrs > 8) {
            overtime = hrs - 8;
            dailyrate = (salary / days) / hours * rate;
            overtimePay = dailyrate * overtime;
//            System.out.println(ratePerHour);
        }
        return overtimePay;
    }

    @Override
    public String toString() {
        return "OverTime{" + "days=" + days + ", hours=" + hours + ", salary=" + salary + ", rate=" + rate + "overtime pay:" + overtimePay(10)+ '}';
    }
    
    
}
