/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package payrollsystem_real;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author student
 */
public class PayrollSystem_REAL {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Person p = new Person();

        p.setfName("Madonna");
        p.setmName("Lopez");
        p.setlName("Real");
        //Just follow this format in setting a date //year/month/date
        p.setBirthday(new Date(1997,7,31));

        System.out.println(p);
        
        OverTime ot = new OverTime();
        ot.overtimePay(10);
        System.out.println(ot);
    }

}
